#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_eth.h"
#include "esp_ota_ops.h"
#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "protocol_examples_common.h"
#include "esp_tls_crypto.h"
#include <esp_http_server.h>

typedef struct
{
  const char *username;
  const char *password;
} basic_auth_info_t;

#define HTTPD_401      "401 UNAUTHORIZED"           /*!< HTTP Response 401 */

#include "mqtt_client.h"
#include "driver/gpio.h"

static bool mqtt_subscribed = false;

static bool     roll_requested = false;

static basic_auth_info_t auth_info = 
{
  .username = "kevin",
  .password = "letmein",
};

//-----------------------------------------------------------------------------
uint32_t system_uptime_s( void )
{
  // esp_timer_get_time returns time in uSec
  return esp_timer_get_time() / 1000000;
}

//-----------------------------------------------------------------------------
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    switch ((esp_mqtt_event_id_t)event_id)
    {
      case MQTT_EVENT_CONNECTED:
          printf( "MQTT Connected to server\n" );
          esp_mqtt_client_subscribe(client, "reef/auto_feeder/request", 0);
          break;

      case MQTT_EVENT_DISCONNECTED:
          mqtt_subscribed = false;
          printf( "MQTT Server Disconnect!\n");
          break;

      case MQTT_EVENT_DATA:
          event->data[event->data_len] = 0;
          printf("Feeder request message data: %s\n", event->data);          
          roll_requested = true;
          break;

      case MQTT_EVENT_ERROR:
          printf( "MQTT Event Error!\n" );
          break;

      case MQTT_EVENT_SUBSCRIBED:
        mqtt_subscribed = true;
        break;
        
      case MQTT_EVENT_UNSUBSCRIBED:
        mqtt_subscribed = false;
        break;
      
      case MQTT_EVENT_PUBLISHED:
      default:
        break;
    }
    fflush(stdout);
}

//-----------------------------------------------------------------------------
static char *http_auth_basic( const char *username, const char *password )
{
  int out;
  char user_info[128];
  static char digest[512];
  size_t n = 0;
  sprintf( user_info, "%s:%s", username, password );

  esp_crypto_base64_encode( NULL, 0, &n, ( const unsigned char * )user_info, strlen( user_info ) );

  // 6: The length of the "Basic " string
  // n: Number of bytes for a base64 encode format
  // 1: Number of bytes for a reserved which be used to fill zero
  if ( sizeof( digest ) > ( 6 + n + 1 ) )
  {
    strcpy( digest, "Basic " );
    esp_crypto_base64_encode( ( unsigned char * )digest + 6, n, ( size_t * )&out, ( const unsigned char * )user_info, strlen( user_info ) );
  }

  return digest;
}

//-----------------------------------------------------------------------------
char auth_buffer[512];
const char html_post_file[] = "\
<style>\n\
.progress {margin: 15px auto;  max-width: 500px;height: 30px;}\n\
.progress .progress__bar {\n\
  height: 100%; width: 1%; border-radius: 15px;\n\
  background: repeating-linear-gradient(135deg,#336ffc,#036ffc 15px,#1163cf 15px,#1163cf 30px); }\n\
 .status {font-weight: bold; font-size: 30px;};\n\
</style>\n\
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.2.1/css/bootstrap.min.css\">\n\
<div class=\"well\" style=\"text-align: center;\">\n\
  <div class=\"btn\" onclick=\"file_sel.click();\"><i class=\"icon-upload\" style=\"padding-right: 5px;\"></i>Upload Firmware</div>\n\
  <div class=\"progress\"><div class=\"progress__bar\" id=\"progress\"></div></div>\n\
  <div class=\"status\" id=\"status_div\"></div>\n\
</div>\n\
<input type=\"file\" id=\"file_sel\" onchange=\"upload_file()\" style=\"display: none;\">\n\
<script>\n\
function upload_file() {\n\
  document.getElementById(\"status_div\").innerHTML = \"Upload in progress\";\n\
  let data = document.getElementById(\"file_sel\").files[0];\n\
  xhr = new XMLHttpRequest();\n\
  xhr.open(\"POST\", \"/ota\", true);\n\
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');\n\
  xhr.upload.addEventListener(\"progress\", function (event) {\n\
     if (event.lengthComputable) {\n\
    	 document.getElementById(\"progress\").style.width = (event.loaded / event.total) * 100 + \"%\";\n\
     }\n\
  });\n\
  xhr.onreadystatechange = function () {\n\
    if(xhr.readyState === XMLHttpRequest.DONE) {\n\
      var status = xhr.status;\n\
      if (status >= 200 && status < 400)\n\
      {\n\
        document.getElementById(\"status_div\").innerHTML = \"Upload accepted. Device will reboot.\";\n\
      } else {\n\
        document.getElementById(\"status_div\").innerHTML = \"Upload rejected!\";\n\
      }\n\
    }\n\
  };\n\
  xhr.send(data);\n\
  return false;\n\
}\n\
</script>";

//-----------------------------------------------------------------------------
static esp_err_t basic_auth_get_handler( httpd_req_t *req )
{
  basic_auth_info_t *basic_auth_info = req->user_ctx;

  size_t buf_len = httpd_req_get_hdr_value_len( req, "Authorization" ) + 1;
  if ( ( buf_len > 1 ) && ( buf_len <= sizeof( auth_buffer ) ) )
  {
    if ( httpd_req_get_hdr_value_str( req, "Authorization", auth_buffer, buf_len ) == ESP_OK )
    {
      char *auth_credentials = http_auth_basic( basic_auth_info->username, basic_auth_info->password );
      if ( !strncmp( auth_credentials, auth_buffer, buf_len ) )
      {
        printf( "Authenticated!\n" );
        httpd_resp_set_status( req, HTTPD_200 );
        httpd_resp_set_hdr( req, "Connection", "keep-alive" );
        httpd_resp_send( req, html_post_file, strlen( html_post_file ) );
        return ESP_OK;
      }
    }
  }

  printf( "Not authenticated\n" );
  httpd_resp_set_status( req, HTTPD_401 );
  httpd_resp_set_hdr( req, "Connection", "keep-alive" );
  httpd_resp_set_hdr( req, "WWW-Authenticate", "Basic realm=\"Hello\"" );
  httpd_resp_send( req, NULL, 0 );

  return ESP_OK;
}

//-----------------------------------------------------------------------------
static esp_err_t ota_post_handler( httpd_req_t *req )
{
  char buf[256];
  httpd_resp_set_status( req, HTTPD_500 );    // Assume failure
  
  int ret, remaining = req->content_len;
  printf( "Receiving\n" );
  
  esp_ota_handle_t update_handle = 0 ;
  const esp_partition_t *update_partition = esp_ota_get_next_update_partition(NULL);
  const esp_partition_t *running          = esp_ota_get_running_partition();
  
  if ( update_partition == NULL )
  {
    printf( "Uh oh, bad things\n" );
    goto return_failure;
  }

  printf( "Writing partition: type %d, subtype %d, offset 0x%08x\n", update_partition-> type, update_partition->subtype, update_partition->address);
  printf( "Running partition: type %d, subtype %d, offset 0x%08x\n", running->type,           running->subtype,          running->address);
  esp_err_t err = ESP_OK;
  err = esp_ota_begin(update_partition, OTA_WITH_SEQUENTIAL_WRITES, &update_handle);
  if (err != ESP_OK)
  {
      printf( "esp_ota_begin failed (%s)", esp_err_to_name(err));
      goto return_failure;
  }
  while ( remaining > 0 )
  {
    // Read the data for the request
    if ( ( ret = httpd_req_recv( req, buf, MIN( remaining, sizeof( buf ) ) ) ) <= 0 )
    {
      if ( ret == HTTPD_SOCK_ERR_TIMEOUT )
      {
        // Retry receiving if timeout occurred
        continue;
      }

      goto return_failure;
    }
    
    size_t bytes_read = ret;
    
    remaining -= bytes_read;
    err = esp_ota_write( update_handle, buf, bytes_read);
    if (err != ESP_OK)
    {
      goto return_failure;
    }
  }

  printf( "Receiving done\n" );

  // End response
  if ( ( esp_ota_end(update_handle)                   == ESP_OK ) && 
       ( esp_ota_set_boot_partition(update_partition) == ESP_OK ) )
  {
    printf( "OTA Success?!\n Rebooting\n" );
    fflush( stdout );

    httpd_resp_set_status( req, HTTPD_200 );
    httpd_resp_send( req, NULL, 0 );
    
    vTaskDelay( 2000 / portTICK_RATE_MS);
    esp_restart();
    
    return ESP_OK;
  }
  printf( "OTA End failed (%s)!\n", esp_err_to_name(err));

return_failure:
  if ( update_handle )
  {
    esp_ota_abort(update_handle);
  }

  httpd_resp_set_status( req, HTTPD_500 );    // Assume failure
  httpd_resp_send( req, NULL, 0 );
  return ESP_FAIL;
}

//-----------------------------------------------------------------------------
esp_err_t http_404_error_handler( httpd_req_t *req, httpd_err_code_t err )
{
  if ( strcmp( "/hello", req->uri ) == 0 )
  {
    httpd_resp_send_err( req, HTTPD_404_NOT_FOUND, "/hello URI is not available" );
    return ESP_OK;    // Return ESP_OK to keep underlying socket open
  }
  else if ( strcmp( "/echo", req->uri ) == 0 )
  {
    httpd_resp_send_err( req, HTTPD_404_NOT_FOUND, "/echo URI is not available" );    
    return ESP_FAIL;    // Return ESP_FAIL to close underlying socket
  }
  
  httpd_resp_send_err( req, HTTPD_404_NOT_FOUND, "404 error" );
  return ESP_FAIL;  // For any other URI send 404 and close socket
}

//-----------------------------------------------------------------------------
static httpd_handle_t start_webserver( void )
{
  httpd_handle_t server = NULL;
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.lru_purge_enable = true;

  // Start the httpd server
  printf( "Starting server on port %d\n", config.server_port );

  if ( httpd_start( &server, &config ) == ESP_OK )
  {
    static const httpd_uri_t ota =
    {
      .uri       = "/ota",
      .method    = HTTP_POST,
      .handler   = ota_post_handler,
      .user_ctx  = NULL
    };
    httpd_register_uri_handler( server, &ota );
    
    static httpd_uri_t root =
    {
      .uri       = "/ota",
      .method    = HTTP_GET,
      .handler   = basic_auth_get_handler,
      .user_ctx  = &auth_info,
    };
    httpd_register_uri_handler( server, &root );
  }
    
  return NULL;
}

//-----------------------------------------------------------------------------
static void disconnect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data )
{
  httpd_handle_t *server = ( httpd_handle_t * ) arg;

  if ( *server )
  {
    printf( "Stopping webserver" );
    httpd_stop( *server );
    *server = NULL;
  }
}

//-----------------------------------------------------------------------------
static void connect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data )
{
  printf("Connected!\n");
  httpd_handle_t *server = ( httpd_handle_t * ) arg;

  if ( *server == NULL )
  {
    printf( "Starting webserver" );
    *server = start_webserver();
  }
}

//-----------------------------------------------------------------------------
static char status_msg[256];
static bool publish_status_msg = false;
static void wifi_task( void *Param )
{
  printf( "Wifi task starting\n" );
  
  httpd_handle_t server = NULL;
  
  esp_netif_init();
  esp_event_loop_create_default();
  example_connect();

  // Register event handlers to stop the server when Wi-Fi is disconnected, and re-start it upon connection
  esp_event_handler_register( IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server );
  esp_event_handler_register( WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server );

  server = start_webserver();
  
  esp_mqtt_client_config_t mqtt_cfg =
  {
      .uri = "mqtt://192.168.0.41",
  };

  esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
  esp_mqtt_client_start(client);
  
  const uint32_t task_delay_ms = 10;
  while(1)
  {    
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    if ( publish_status_msg )
    {
      esp_mqtt_client_publish( client, "reef/auto_feeder/status", status_msg, 0, 1, 0);
      publish_status_msg = false;
    }
  }
}

//-----------------------------------------------------------------------------
static void ota_task(void *Param)
{
  const esp_partition_t *running = esp_ota_get_running_partition();
  esp_ota_img_states_t ota_state;
  if ( esp_ota_get_state_partition(running, &ota_state) == ESP_OK )
  {
    if (ota_state == ESP_OTA_IMG_PENDING_VERIFY)
    {
      // Validate image some how, then call:
      esp_ota_mark_app_valid_cancel_rollback();
      // If needed: esp_ota_mark_app_invalid_rollback_and_reboot();
    }
  }
  
  const uint32_t task_delay_ms = 10;
  while(1)
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void send_mqtt_status_msg( uint32_t roll_cnt, bool motor_running )
{
  static uint32_t status_msg_id = 0;
  
  // IP address.
  tcpip_adapter_ip_info_t ipInfo; 
  tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);
  
  uint8_t ip_addr[4] = {
    ( ipInfo.ip.addr >> 24 ) & 0xFF,
    ( ipInfo.ip.addr >> 16 ) & 0xFF,
    ( ipInfo.ip.addr >>  8 ) & 0xFF,
    ( ipInfo.ip.addr >>  0 ) & 0xFF
  };

  char *p_msg = status_msg;
  status_msg_id++;
  p_msg += sprintf( p_msg, "{ \"message_id\":%i,",           status_msg_id );
  p_msg += sprintf( p_msg, "\"ip_addr\":\"%u.%u.%u.%u.\",",  ip_addr[3], ip_addr[2], ip_addr[1], ip_addr[0] );
  p_msg += sprintf( p_msg, "\"current_time\":%u,",           system_uptime_s() );
  p_msg += sprintf( p_msg, "\"motor_running\":\"%s\",",      motor_running ? "True" : "False" );
  p_msg += sprintf( p_msg, "\"roll_cnt\":%u",                roll_cnt );
  
  p_msg += sprintf( p_msg, "}" );
  
  publish_status_msg = true;
}


//-----------------------------------------------------------------------------
static void application_task(void *Param)
{
  printf( "Reef Autofeer app init\n");

#define ROLL_STATUS   ( 2 )
#define ROLL_DRIVE    ( 6 )

  gpio_config_t io_conf;
  io_conf.pin_bit_mask = ( 1ULL<< ROLL_STATUS );
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.pull_up_en = 1;
  io_conf.pull_down_en = 0;
  gpio_config(&io_conf);
  
  io_conf.pin_bit_mask = ( 1ULL<< ROLL_DRIVE );
  io_conf.mode = GPIO_MODE_OUTPUT;
  gpio_set_level( ROLL_DRIVE, 1 );
  gpio_config(&io_conf);

  uint32_t roll_cnt      = 0;
  const uint32_t task_delay_ms = 100;

  while ( 1 )
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    
    if ( roll_requested )
    {
      send_mqtt_status_msg( roll_cnt, true );
      
      bool start_tripped = gpio_get_level( ROLL_STATUS );
      
      gpio_set_level( ROLL_DRIVE, 0 );
      
      while ( start_tripped && gpio_get_level( ROLL_STATUS ) )
      {
        vTaskDelay( 1 / portTICK_RATE_MS);
      }
      
      // 100 mSec for a little debounce
      vTaskDelay( 100 / portTICK_RATE_MS);

      while ( !gpio_get_level( ROLL_STATUS ) )
      {
        vTaskDelay( 1 / portTICK_RATE_MS);
      }
      
      // Extra time to square up
      vTaskDelay( 150 / portTICK_RATE_MS);
      
      gpio_set_level( ROLL_DRIVE, 1 );

      roll_cnt++;
      roll_requested = false;
    }
    
    static uint32_t time_since_last_update_ms = 0;
    static uint32_t last_roll_cnt = 0;
    
    time_since_last_update_ms += task_delay_ms;
    
    if ( ( time_since_last_update_ms > 5000 ) || ( last_roll_cnt != roll_cnt ) )
    {
      send_mqtt_status_msg( roll_cnt, false );

      time_since_last_update_ms = 0;
      last_roll_cnt = roll_cnt;
    }
  }
}

//-----------------------------------------------------------------------------
void app_main( void )
{ 
  esp_err_t error;
  printf( "****************************\n" );
  printf( "Application task starting\n" );

  // Initialize NVS.
  error = nvs_flash_init();
  if ( ( error == ESP_ERR_NVS_NO_FREE_PAGES ) || ( error == ESP_ERR_NVS_NEW_VERSION_FOUND ) )
  {
    // Don't bother checking return codes, it's not like we can do anything about failures here anyways
    nvs_flash_erase();
    nvs_flash_init();
  }
  
  // Put all the wifi stuff in a separate task so that we don't have to wait for a connection
  xTaskCreate( wifi_task, "wifi_task", 4096, NULL, 0, NULL );
  xTaskCreate( ota_task, "ota_task", 8192, NULL, 5, NULL);
  xTaskCreate( application_task, "app_task", 8192, NULL, 5, NULL);
  
  const uint32_t task_delay_ms = 10;
  while(1)
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    fflush(stdout);
  }
}
